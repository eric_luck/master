//
//  DVTRestClient.swift
//  WeatherForecast
//
//  Created by Rahul Shirphule on 2019/02/12.
//  Copyright © 2019 DVT. All rights reserved.
//

import Foundation

class DVTRestClient: NSObject {
    
    let hexColors:[[String : Int]] = [
        ["Sunny": 0x47AB2F],
        ["Cloudy": 0x54717A],
        ["Rainy": 0x57575D]
    ]
    
    let weatherIcons:[[String : String]] = [
        ["Sunny": "clear"],
        ["Cloudy": "partlysunny"],
        ["Rainy": "rain"]
    ]
    
    let forestImages:[[String : String]] = [
        ["Sunny": "forest_sunny"],
        ["Cloudy": "forest_cloudy"],
        ["Rainy": "forest_rainy"]
    ]
    
    let seaImages:[[String]] = [
        ["Sunny", "sea_sunnypng"],
        ["Cloudy", "sea_cloudy"],
        ["Rainy", "sea_rainy"]
    ]
    
}
