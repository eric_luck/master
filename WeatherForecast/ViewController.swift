//
//  ViewController.swift
//  WeatherForecast
//
//  Created by Rahul Shirphule on 2019/02/11.
//  Copyright © 2019 DVT. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController , UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var lblWeather: UILabel!
    @IBOutlet weak var lblMinimumTemp: UILabel!
    @IBOutlet weak var lblCurrentTemp: UILabel!
    @IBOutlet weak var tblWeatherForecast: UITableView!
    @IBOutlet weak var lblMaximumTemp: UILabel!
    
    @IBOutlet weak var temperatureContainer: UIView!
    var isForestThemeSelected = true;
    var currentLocation : CLLocation = CLLocation();
    var gettingWeatherForLocation : Bool = false;
    var foreCastDates = NSMutableArray();
    var listOfForecastData = NSMutableArray();
    let DegreeCelsiusConstant = 14;
    var cellHeight = 0;
    var locationManager = CLLocationManager()

    let contacts:[[String]] = [
        ["Tuesday"],
        ["Wednesday"],
        ["Thurday"],
        ["Friday"],
        ["Saturday"]
    ]
    
    let APIKey = "37b520286cf7f45bc26c44ce46401c01";

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization();
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest // You can change the locaiton accuary here.
            locationManager.startUpdatingLocation()
            
        }
        else {
            
            self.alertToGivePermission();
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let height =  self.tblWeatherForecast.frame.height;
        cellHeight = Int(height / 5.5);
        reloadTableView();
    }
    
    func reloadTableView() {
        tblWeatherForecast.reloadData()
        tblWeatherForecast.reloadSections([0], with: UITableView.RowAnimation.middle)
        tblWeatherForecast.allowsSelection = false;
    }
    
    // Print out the location to the console
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print(location.coordinate)
            currentLocation = location;
            
            manager.stopUpdatingLocation();
            locationManager.stopUpdatingLocation();
            if !gettingWeatherForLocation {
                gettingWeatherForLocation = true;
                
                DispatchQueue.global(qos: .background).async {
                    
                    if Reachability.isConnectedToNetwork(){
                        self.getWeatherForecastForToday();
                        self.getWeatherForcastForWeek();
                        
                    }else{
                        self.showAlertWithText(message: "You are offline", title: "Alert")
                    }
                }
            }
        }
    }
    
    // If we have been deined access give the user the option to change it
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
        else if status == CLAuthorizationStatus.notDetermined {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    // Show the popup to the user if we have been deined access
    func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Please allow app to use location services",
                                                message: "In order to check weather we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

    func showAlertWithText(message:String,title:String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func alertToGivePermission() {
        
        let alert = UIAlertController(title: "Alert", message: "Please allow app to use location services", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { action in
            print("Yay! settings")
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) { UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            }
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    var selectedColor : UIColor!;
    
    func getWeatherForecastForToday() {
        
        let stringUrl = "http://api.openweathermap.org/data/2.5/weather?lat="+currentLocation.coordinate.latitude.description+"&lon="+currentLocation.coordinate.longitude.description+"&APPID="+self.APIKey;
        
        let url = URL(string: stringUrl)!
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    self.gettingWeatherForLocation = false;
                    
                    // Convert the data to JSON
                    let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    
                    let arrWeather = jsonSerialized?["weather"] as! NSArray;
                    if arrWeather.count > 0 {
                        let obj = arrWeather.object(at: 0) as! NSDictionary;
                        let weather = obj.value(forKey: "main");
//                        print(weather);
                       
                        let tempratureDict = jsonSerialized?["main"] as! NSDictionary;
                        let temp = Int(tempratureDict.value(forKey: "temp") as! Double);
                        let temp_min = Int(tempratureDict.value(forKey: "temp_min")  as! Double);
                        let temp_max = Int(tempratureDict.value(forKey: "temp_max")  as! Double);
                        
                        DispatchQueue.main.async {
                            self.lblWeather.text = weather as? String;
                            self.lblTemperature.text = "\(temp/self.DegreeCelsiusConstant)"+"°";
                            self.lblCurrentTemp.text =  "\(temp/self.DegreeCelsiusConstant)"+"°";
                            self.lblMinimumTemp.text =  "\(temp_min/self.DegreeCelsiusConstant)"+"°";
                            self.lblMaximumTemp.text =  "\(temp_max/self.DegreeCelsiusConstant)"+"°";
                            
                            if (weather as! String == "Clouds") {
                                self.backgroundImage.image = UIImage.init(named: "forest_cloudy")
                                self.selectedColor =  UIColor.init(rgb: 0x54717A)
                            }
                            else if (weather as! String == "Clear") {
                                self.backgroundImage.image = UIImage.init(named: "forest_sunny")
                                 self.selectedColor =  UIColor.init(rgb: 0x47AB2F)
                            } else {
                                self.backgroundImage.image = UIImage.init(named: "forest_rainy")
                                 self.selectedColor =  UIColor.init(rgb: 0x57575D)
                            }
                            
                            self.temperatureContainer.backgroundColor =  self.selectedColor;
                        }
                    }
                    
                    self.dismiss(animated: false, completion: nil)

                }  catch let error as NSError {
                    self.gettingWeatherForLocation = false;
                    print(error.localizedDescription)
                    self.dismiss(animated: false, completion: nil)

                }
            } else if let error = error {
                self.gettingWeatherForLocation = false;
                print(error.localizedDescription)
                self.dismiss(animated: false, completion: nil)

            }
        }
        
        task.resume()

        
    }
    
    func getWeatherForcastForWeek() {
        
        let stringUrl = "http://api.openweathermap.org/data/2.5/forecast?lat="+currentLocation.coordinate.latitude.description+"&lon="+currentLocation.coordinate.longitude.description+"&APPID="+self.APIKey;
        
        let url = URL(string: stringUrl)!
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if let data = data {
                do {
                    
                    self.gettingWeatherForLocation = false;
                    
                    // Convert the data to JSON
                    let jsonSerialized = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                    
                    let listWeather = jsonSerialized?["list"] as! NSArray;
                    if listWeather.count > 0 {
                        
                        for item in listWeather {
                            
                            let dict = item as! NSDictionary;
                            let weatherObject = NSMutableDictionary();
                            
                            let str_dt_txt = dict.object(forKey: "dt_txt") as! NSString;
                            let arrDate = str_dt_txt.components(separatedBy: " ") as NSArray
                            let dt_txt = arrDate.object(at: 0);
                            
                            if self.foreCastDates.count == 0 || (!self.foreCastDates.contains(dt_txt)) {
                                weatherObject.setValue(dt_txt, forKey: "dt_txt")
                                let main = dict.object(forKey: "main") as! NSDictionary;
                                let temp = main.object(forKey: "temp") as! Double
                                
                                let listOfWeather = dict.object(forKey: "weather") as! NSArray;
                                let weather = listOfWeather.object(at: 0) as! NSDictionary;
                                
                                let overallWeather = weather.object(forKey: "main")
                                
                                weatherObject.setValue(temp, forKey: "temp")
                                weatherObject.setValue(overallWeather, forKey: "overallWeather")
                                
                                let dateOfWeek = self.getDateFromString(strDate: dt_txt as! String) ;
                                self.foreCastDates.add(dt_txt);
                                
                                let day = dateOfWeek.dayOfWeek()
                                weatherObject.setValue(day, forKey: "day")

                                self.listOfForecastData.add(weatherObject)

                            }
                           
                        }
                        
                        DispatchQueue.main.async {
                            self.reloadTableView();
                        }
                    }
                    
                     self.dismiss(animated: false, completion: nil)
                    
                }  catch let error as NSError {
                    self.gettingWeatherForLocation = false;
                    print(error.localizedDescription)
                     self.dismiss(animated: false, completion: nil)
                }
            } else if let error = error {
                self.gettingWeatherForLocation = false;
                print(error.localizedDescription)
                 self.dismiss(animated: false, completion: nil)
            }
        }
        
        task.resume()
        
        
    }
    
    func themeSelection() {
        let alert = UIAlertController(title: "Alert", message: "Please select theme", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Forest", style: UIAlertAction.Style.default, handler: { action in
            print("Yay! Forest")
        }))
        
        alert.addAction(UIAlertAction(title: "Sea", style: UIAlertAction.Style.default, handler: { action in
            print("Yay! Sea")
            self.backgroundImage.image = UIImage.init(imageLiteralResourceName: "sea_sunnypng");
            self.isForestThemeSelected = false;
            
            self.reloadTableView();
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if cellHeight > 0 {
            return CGFloat(cellHeight);
        }
        else {
            return 40;
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath) as! WeatherInfoCell
        
//        print("\(#function) --- section = \(indexPath.section), row = \(indexPath.row)")
        
        if self.listOfForecastData.count > 0 {
            
            let weatherData = self.listOfForecastData.object(at: indexPath.row) as! NSDictionary;
            
            cell.lblDay?.text = weatherData.object(forKey: "day") as? String
            let tempInDegree = weatherData.object(forKey: "temp") as? Double;
            let temp = Int(tempInDegree ?? 0.0) / self.DegreeCelsiusConstant;
            let strTemp = "\(temp)" + "°"
            cell.lblTemperature?.text = strTemp;
            
            if (weatherData.object(forKey: "overallWeather") as! String == "Clouds") {
                cell.weatherIcon.image = UIImage(named: "partlysunny")
            }
            else if (weatherData.object(forKey: "overallWeather") as! String == "Clear") {
                cell.weatherIcon.image = UIImage(named: "clear")
            } else {
                cell.weatherIcon.image = UIImage(named: "rain")
            }

            cell.backgroundColor = selectedColor;
            
        } else {
            cell.lblDay?.text = contacts[indexPath.row][0]
        }
        
        if !isForestThemeSelected {
//            let color2 = UIColor(rgb: 51)
            cell.backgroundColor = UIColor.init(rgb: 0x54717A)
        }
        
        return cell
    }
    
    func getDateFromString(strDate : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: strDate)
        return date ?? Date();
    }
    
    var alert : UIAlertController!;
    func presentLoader()  {
        
         alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        present(alert, animated: true, completion: nil)
        
    }

}

class WeatherInfoCell: UITableViewCell {
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var lblTemperature: UILabel!
}


extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}

extension Date {
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
        // or use capitalized(with: locale) if you want
    }
}
